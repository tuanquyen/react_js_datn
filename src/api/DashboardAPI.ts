import axios from 'axios'
import { getJwtToken, URL_BE } from './secure'
import { useQuery } from '@tanstack/react-query'

//API 1: --------------------------------------------------------------------------------
interface GetRevenueDashboardAPIParamsType {
  mode: 'THIS_MONTH' | 'LAST_MONTH' | 'ADVANCED'
  startDateInput?: number
  endDateInput?: number
}

interface GetRevenueDashboardAPIType {
  date: string
  totalAmount: number
}

export const GetRevenueDashboardAPI = ({
  mode,
  startDateInput,
  endDateInput
}: GetRevenueDashboardAPIParamsType) => {
  return useQuery<GetRevenueDashboardAPIType[]>({
    queryKey: ['GetRevenueDashboard', mode, startDateInput, endDateInput],
    queryFn: async () => {
      const res = await axios({
        method: 'get',
        url: `${URL_BE}/staff/revenue`,
        params: { mode, startDateInput, endDateInput },
        headers: {
          Authorization: `Bearer ${getJwtToken()}`
        }
      })

      if (res.status === 200) {
        return res.data.data
      }
    }
  })
}

//API 2: ------------------------------------------------------------------------------------------------
interface GetBestSellingDashboardAPIType {
  id: number
  name: string
  totalQuantity: number
}

export const GetBestSellingDashboardAPI = ({
  mode
}: {
  mode: 'THIS_MONTH' | 'LAST_MONTH' | 'THIS_WEEK' | 'LAST_WEEK'
}) => {
  return useQuery<GetBestSellingDashboardAPIType[]>({
    queryKey: ['GetBestSellingDashboard', mode],
    queryFn: async () => {
      const res = await axios({
        method: 'get',
        url: `${URL_BE}/staff/best-selling`,
        params: {
          mode: mode
        },
        headers: {
          Authorization: `Bearer ${getJwtToken()}`
        }
      })
      if (res.status === 200) {
        return res.data.data
      }
    }
  })
}

//API 3: ---------------------------------------------------------
interface CountValueType {
  count: number
  value: number
}

export interface GetOverViewDashboardAPIType {
  waiting: CountValueType
  confirm: CountValueType
  shipping: CountValueType
  done: CountValueType
  cancel: CountValueType
}
export const GetOverViewDashboardAPI = () => {
  return useQuery<GetOverViewDashboardAPIType>({
    queryKey: ['GetOverViewDashboard'],
    queryFn: async () => {
      const res = await axios({
        method: 'get',
        url: `${URL_BE}/staff/over-view`,
        headers: {
          Authorization: `Bearer ${getJwtToken()}`
        }
      })

      const states = ['WAITING', 'CONFIRM', 'SHIPPING', 'DONE', 'CANCEL']

      const result: GetOverViewDashboardAPIType =
        {} as GetOverViewDashboardAPIType

      states.forEach((state) => {
        const countItem = res.data.data?.count?.find(
          (item) => item.state === state
        ) || {
          state,
          count: 0
        }
        const valueItem = res.data.data?.value?.find(
          (item) => item.state === state
        ) || {
          state,
          value: 0
        }
        result[state.toLowerCase()] = {
          count: countItem.count,
          value: valueItem.value
        }
      })

      return result
    }
  })
}
