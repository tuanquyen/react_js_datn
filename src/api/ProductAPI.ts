import { getJwtToken, URL_BE } from './secure'
import { useMutation, useQuery } from '@tanstack/react-query'
import axios from 'axios'
import { toastErrorMessage } from '@/utils/errorMessage'
import { useState } from 'react'

export interface AddTitleProductsType {
  id: number
  name: string
  image: string | null
  title: string
}

// // API 1: ----------------------------------------------------
// export const GetAllProductAPI = (params?) => {
//   return useQuery({
//     queryKey: ['GetAllProductAPI'],
//     queryFn: async () => {
//       const res = await axios({
//         method: 'get',
//         url: `${URL_BE}/product/all`,
//         params: params
//       })
//       if (res.status === 200) {
//         const productListFormat = res.data.data.map((product) => {
//           return {
//             ...product,
//             created_at: formatDateTime(product.created_at)
//           }
//         })
//         return productListFormat
//       }
//     }
//   })
// }

// API 2: ------------------------------------------
export interface GetProductDetailAPIType {
  id: string
  name: string
  manufacturer: string
  material: string
  category_id: number
  pattern: string
  style: string
  avg_rating: number
  created_at: string
  others: { value: string; name: string }[]
  product_image: {
    id: number
    color_id: number
    image: string
  }[]
  variants: []
  count_bought: number
  count_review: number
}

export const GetProductDetailAPI = (product_id: number) => {
  return useQuery<GetProductDetailAPIType>({
    queryKey: ['GetProductDetailAPI', product_id],
    queryFn: async () => {
      const res = await axios({
        method: 'get',
        url: `${URL_BE}/product/detail`,
        params: { product_id: product_id },
        headers: {
          Authorization: `Bearer ${getJwtToken()}`
        }
      })
      return res.data.data
    }
  })
}

export interface CreateProductAPIBodyType {
  name: string
  manufacturer?: string
  pattern?: string
  style?: string
  material?: string
  category_id: number
  others?: { name: string; value: string }[]
}
export const CreateProductAPI = async (data: CreateProductAPIBodyType) => {
  return await axios({
    method: 'post',
    data: data,
    url: `${URL_BE}/product`,
    headers: {
      Authorization: `Bearer ${getJwtToken()}`
    }
  })
}

export const UpdateProductAPI = async (data) => {
  return await axios({
    method: 'patch',
    data: data,
    url: `${URL_BE}/product`,
    headers: {
      Authorization: `Bearer ${getJwtToken()}`
    }
  })
}

export const DeleteProductAPI = async (product_id) => {
  return await axios({
    method: 'delete',
    params: { id: product_id },
    url: `${URL_BE}/product`,
    headers: {
      Authorization: `Bearer ${getJwtToken()}`
    }
  })
}

export interface FilterProductAPIBodyType {
  min_price?: number
  max_price?: number
  rating?: number
  sort_by?: string
  category_id?: number
  search?: string
}

export interface FilterProductAPIType {
  id: number
  image: string | null
  name: string
  price_export: number
  price_sale?: number
  status: string
}

export const GetAllProductAPI = () => {
  const [filteredProducts, setFilteredProducts] = useState<
    FilterProductAPIType[]
  >([])

  const FilterProductAPI = async (data?: FilterProductAPIBodyType) => {
    return await axios({
      method: 'post',
      url: `${URL_BE}/product/filter`,
      data: { ...data, limit: 100 },
      headers: {
        Authorization: `Bearer ${getJwtToken()}`
      }
    })
  }

  const mutation = useMutation({
    mutationFn: FilterProductAPI,
    onSuccess: (res) => {
      const products = res?.data?.data?.map((product) => {
        const variant = product?.variants[0]
        const productImage = product?.product_image[0]

        return {
          id: product?.id,
          name: product?.name,
          image: productImage?.image,
          price_sale: variant?.price_sale,
          price_export: variant?.price_export,
          status: variant?.price_export > variant?.price_sale ? 'sale' : ''
        }
      })

      setFilteredProducts(products)
    },
    onError: (err) => {
      toastErrorMessage(err)
    }
  })

  return { filteredProducts, mutation }
}

export const AddVariantImageAPI = async (data) => {
  return await axios({
    method: 'patch',
    data: data,
    url: `${URL_BE}/product/variant/image`,
    headers: {
      Authorization: `Bearer ${getJwtToken()}`
    }
  })
}

export const DeleteVariantImageAPI = async (id) => {
  return await axios({
    method: 'delete',
    params: { id },
    url: `${URL_BE}/product/variant/image`,
    headers: {
      Authorization: `Bearer ${getJwtToken()}`
    }
  })
}
