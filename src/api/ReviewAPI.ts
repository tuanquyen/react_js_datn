import axios from 'axios'
import { getJwtToken, URL_BE } from './secure'
import { useQuery } from '@tanstack/react-query'

export const DeleteReviewProductAPI = async (review_id) => {
  return await axios({
    method: 'delete',
    params: { review_id: review_id },
    url: `${URL_BE}/review`,
    headers: {
      Authorization: `Bearer ${getJwtToken()}`
    }
  })
}

interface GetAllReviewAPIParams {
  product_id?: number
  user_id?: number
  page?: number
  limit?: number
  is_disabled?: boolean
}
export const GetAllReviewAPI = (params: GetAllReviewAPIParams) => {
  return useQuery({
    queryKey: [
      'GetReviewOfProductDetailAPI',
      params.product_id,
      params.user_id,
      params.page,
      params.limit ?? 100,
      params.is_disabled
    ],
    queryFn: async () => {
      const res = await axios({
        method: 'get',
        url: `${URL_BE}/review`,
        params: {
          product_id: params.product_id,
          user_id: params.user_id,
          page: params.page,
          limit: params.limit ?? 100,
          is_disabled: params.is_disabled
        },
        headers: {
          Authorization: `Bearer ${getJwtToken()}`
        }
      })
      return res.data?.data
    }
  })
}
