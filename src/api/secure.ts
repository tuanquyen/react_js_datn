export const getJwtToken = () => {
  const jwtToken = localStorage.getItem('jwt')
  if (!jwtToken) {
    throw new Error('JWT token not found in localStorage')
  }
  return jwtToken
}

export const URL_BE = import.meta.env.VITE_REACT_APP_BE_URL
