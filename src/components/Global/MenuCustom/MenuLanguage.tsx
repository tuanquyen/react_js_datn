import React, { useEffect, useState } from 'react'
import MenuItem from '@mui/material/MenuItem'
import Box from '@mui/material/Box'
import Translate from '@mui/icons-material/Translate'
import Menu from '@mui/material/Menu'
import { useTranslation } from 'react-i18next'
import { Typography } from '@mui/material'

export default function MenuLanguage() {
  const { i18n } = useTranslation('translation')

  const [anchorEl, setAnchorEl] = useState(null)

  const [language, setLanguage] = useState(
    localStorage.getItem('language') || 'en'
  )

  useEffect(() => {
    i18n.changeLanguage(language)
  }, [language, i18n])

  const open = Boolean(anchorEl)

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget)
  }

  const handleClose = () => {
    setAnchorEl(null)
  }

  const handleLanguageChange = (newLanguage: string) => {
    localStorage.setItem('language', newLanguage)
    setLanguage(newLanguage)
    handleClose()
  }

  return (
    <Box component="div">
      <Box
        onClick={handleClick}
        className="hover:cursor-pointer"
        sx={{
          background: (theme) => theme.palette._white_121212.main
        }}
      >
        <Typography>
          {language === 'en' ? (
            'ENG'
          ) : language === 'vi' ? (
            'VIE'
          ) : (
            <Translate />
          )}
        </Typography>
      </Box>
      <Menu
        disableScrollLock={true}
        id="demo-customized-menu"
        MenuListProps={{
          'aria-labelledby': 'demo-customized-button'
        }}
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
      >
        <MenuItem
          selected={language === 'vi'}
          onClick={() => handleLanguageChange('vi')}
          disableRipple
        >
          <Typography>
            Tiếng Việt
            <Typography sx={{ color: 'rgb(105, 117, 134)', paddingLeft: 1 }}>
              (VN)
            </Typography>
          </Typography>
        </MenuItem>
        <MenuItem
          selected={language === 'en'}
          onClick={() => handleLanguageChange('en')}
          disableRipple
        >
          <Typography>
            English{' '}
            <Typography sx={{ color: 'rgb(105, 117, 134)', paddingLeft: 1 }}>
              {' '}
              (UK)
            </Typography>
          </Typography>
        </MenuItem>
      </Menu>
    </Box>
  )
}
