import React, { useState } from 'react'
import { Box, Stack } from '@mui/material'
import { Close, Menu } from '@mui/icons-material'
import Notifications from '@mui/icons-material/Notifications'
import MenuProfile from '@/components/Global/MenuCustom/MenuProfile'
import MenuLanguage from './Global/MenuCustom/MenuLanguage'
import ModeSelect from './Global/MenuCustom/ModeSelect'
import Sidebar from './Sidebar'

const MenuNav = () => {
  const [isOpen, setIsOpen] = useState(false)

  const openNavbar = () => {
    setIsOpen(true)
  }

  const closeNavbar = () => {
    setIsOpen(false)
  }

  return (
    <>
      <Box
        className={`fixed top-0 left-0 z-100 ${
          isOpen ? 'w-[256px]' : 'w-0'
        } h-screen bg-gray-800 transition-all duration-300 ease-in-out overflow-hidden`}
      >
        {isOpen && (
          <>
            <Box>
              <Box className="text-white">Heee</Box>

              <Sidebar clickMenu={false} />
              <Box className="text-white">Hiiii</Box>
            </Box>
            <Box className="absolute top-4 right-4">
              <button onClick={closeNavbar}>
                <Close />
              </button>
            </Box>
          </>
        )}
      </Box>
      <Menu
        color="error"
        sx={{ display: { lg: 'none' } }}
        onClick={openNavbar}
      />
    </>
  )
}

export default function Header({ clickMenu, setClickMenu }) {
  return (
    <Box
      sx={{
        height: (theme) => theme.dimension.header_height,
        background: (theme) => theme.palette._white_121212.main,
        left: { xs: 0, lg: clickMenu === true ? 0 : 256 }
      }}
      component="div"
      className={`fixed top-0 right-0 flex items-center justify-between  pl-[20px] shadow-sm  z-[200]
        }`}
    >
      <Box component="div" className="flex items-center gap-[60px]">
        <Box component="div">
          <Menu
            sx={{
              display: { xs: 'none', lg: 'block' },
              '&:hover': {
                cursor: 'pointer'
              }
            }}
            className=" text-primary"
            onClick={() => setClickMenu(!clickMenu)}
          />
          <Box
            sx={{
              display: { xs: 'block', lg: 'none' },
              '&:hover': {
                cursor: 'pointer'
              }
            }}
          >
            <MenuNav />
          </Box>
        </Box>
      </Box>
      <Stack direction="row" spacing={5}>
        <MenuLanguage />
        <ModeSelect />
        {/*    */}
        <Box component="div">
          <Notifications
            sx={{
              color: (theme) => theme.palette.secondary.main
            }}
          />
        </Box>
        <MenuProfile />
      </Stack>
    </Box>
  )
}
