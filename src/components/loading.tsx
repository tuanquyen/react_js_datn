import CircularProgress from '@mui/material/CircularProgress'
import Box from '@mui/material/Box'
import Alert from '@mui/material/Alert'

export default function LoadingComponent({
  loadingLabel,
  isError,
  errorLabel
}: {
  loadingLabel?: string
  isError?: boolean
  errorLabel?: string
}) {
  // Or a custom loading skeleton component
  return (
    <Box className="flex flex-col items-center gap-[10px]">
      {isError ? (
        <Alert severity="error">{errorLabel}</Alert>
      ) : (
        <>
          <Box>
            <CircularProgress className="!text-primary" />
          </Box>
          <Box className="text-gray-500 text-[12px]">{loadingLabel}</Box>
        </>
      )}
    </Box>
  )
}
