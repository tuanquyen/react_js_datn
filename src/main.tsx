import React from 'react'
import ReactDOM from 'react-dom/client'
import CssBaseline from '@mui/material/CssBaseline'
import App from './App'
import '@/assets/styles/global.css'
import { ToastContainer } from 'react-toastify'
import Provider from '@/components/Provider'
import 'react-toastify/dist/ReactToastify.css'
import ThemeMUI from '@/components/ThemeMUI'
import { LocalizationProvider } from '@mui/x-date-pickers'
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs'
import { QueryClient, QueryClientProvider } from '@tanstack/react-query'
import { I18nextProvider } from 'react-i18next'
import i18n from '@/translation/i18n'

const queryClient = new QueryClient()

ReactDOM.createRoot(document.getElementById('root')!).render(
  <QueryClientProvider client={queryClient}>
    <ThemeMUI>
      <LocalizationProvider dateAdapter={AdapterDayjs}>
        <Provider>
          <CssBaseline />
          <I18nextProvider i18n={i18n}>
            <App />
          </I18nextProvider>
          <ToastContainer
            position="top-right"
            autoClose={5000}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnFocusLoss
            draggable
            pauseOnHover
            theme="light"
          />
          {/* Same as */}
          <ToastContainer />
        </Provider>
      </LocalizationProvider>
    </ThemeMUI>
  </QueryClientProvider>
)
