import { useSpring, animated } from '@react-spring/web'
import SvgIcon, { SvgIconProps } from '@mui/material/SvgIcon'
import { TransitionProps } from '@mui/material/transitions'
import Collapse from '@mui/material/Collapse'
import { alpha, styled } from '@mui/material/styles'
import { TreeView } from '@mui/x-tree-view/TreeView'
import {
  TreeItem,
  TreeItemProps,
  treeItemClasses
} from '@mui/x-tree-view/TreeItem'
import Button from '@mui/material/Button'
import DeleteOutlinedIcon from '@mui/icons-material/DeleteOutlined'
import {
  FormControl,
  Box,
  InputLabel,
  MenuItem,
  Modal,
  Select,
  TextField,
  Typography,
  Stack,
  FormHelperText
} from '@mui/material'
import { styleModal } from '@/components/Global/StyleModal'
import React, { useState } from 'react'
import { toast } from 'react-toastify'
import { useTreeItem, TreeItemContentProps } from '@mui/x-tree-view/TreeItem'
import clsx from 'clsx'
import { Edit } from '@mui/icons-material'
import { Controller, useForm } from 'react-hook-form'
import { UploadIcon } from '@/assets/icons'
import { convertCategory } from '@/utils/convertData'
import LoadingComponent from '@/components/loading'
import { toastErrorMessage } from '@/utils/errorMessage'
import { useMutation } from '@tanstack/react-query'
import LoadingButton from '@mui/lab/LoadingButton'
import { DeleteCategoryAPI, UpdateCategoryAPI } from '@/api/CategoryAPI'
import { useTranslation } from 'react-i18next'

function convertData(inputData) {
  const resultMap = new Map()
  const resultArray = []

  inputData?.forEach((item) => {
    resultMap.set(item.id, { ...item, children: [] })
  })

  resultMap.forEach((node) => {
    const parentId = node.parent_category ? node.parent_category.id : null

    if (parentId !== null) {
      const parentNode = resultMap.get(parentId)
      if (parentNode) {
        parentNode.children.push(node)
      }
    } else {
      resultArray.push(node as never)
    }
  })

  return resultArray
}

function MinusSquare(props: SvgIconProps) {
  return (
    <SvgIcon fontSize="inherit" style={{ width: 14, height: 14 }} {...props}>
      {/* tslint:disable-next-line: max-line-length */}
      <path d="M22.047 22.074v0 0-20.147 0h-20.12v0 20.147 0h20.12zM22.047 24h-20.12q-.803 0-1.365-.562t-.562-1.365v-20.147q0-.776.562-1.351t1.365-.575h20.147q.776 0 1.351.575t.575 1.351v20.147q0 .803-.575 1.365t-1.378.562v0zM17.873 11.023h-11.826q-.375 0-.669.281t-.294.682v0q0 .401.294 .682t.669.281h11.826q.375 0 .669-.281t.294-.682v0q0-.401-.294-.682t-.669-.281z" />
    </SvgIcon>
  )
}

function PlusSquare(props: SvgIconProps) {
  return (
    <SvgIcon fontSize="inherit" style={{ width: 14, height: 14 }} {...props}>
      {/* tslint:disable-next-line: max-line-length */}
      <path d="M22.047 22.074v0 0-20.147 0h-20.12v0 20.147 0h20.12zM22.047 24h-20.12q-.803 0-1.365-.562t-.562-1.365v-20.147q0-.776.562-1.351t1.365-.575h20.147q.776 0 1.351.575t.575 1.351v20.147q0 .803-.575 1.365t-1.378.562v0zM17.873 12.977h-4.923v4.896q0 .401-.281.682t-.682.281v0q-.375 0-.669-.281t-.294-.682v-4.896h-4.923q-.401 0-.682-.294t-.281-.669v0q0-.401.281-.682t.682-.281h4.923v-4.896q0-.401.294-.682t.669-.281v0q.401 0 .682.281t.281.682v4.896h4.923q.401 0 .682.281t.281.682v0q0 .375-.281.669t-.682.294z" />
    </SvgIcon>
  )
}

function CloseSquare(props: SvgIconProps) {
  return (
    <SvgIcon
      className="close"
      fontSize="inherit"
      style={{ width: 14, height: 14 }}
      {...props}
    >
      {/* tslint:disable-next-line: max-line-length */}
      <path d="M17.485 17.512q-.281.281-.682.281t-.696-.268l-4.12-4.147-4.12 4.147q-.294.268-.696.268t-.682-.281-.281-.682.294-.669l4.12-4.147-4.12-4.147q-.294-.268-.294-.669t.281-.682.682-.281.696 .268l4.12 4.147 4.12-4.147q.294-.268.696-.268t.682.281 .281.669-.294.682l-4.12 4.147 4.12 4.147q.294.268 .294.669t-.281.682zM22.047 22.074v0 0-20.147 0h-20.12v0 20.147 0h20.12zM22.047 24h-20.12q-.803 0-1.365-.562t-.562-1.365v-20.147q0-.776.562-1.351t1.365-.575h20.147q.776 0 1.351.575t.575 1.351v20.147q0 .803-.575 1.365t-1.378.562v0z" />
    </SvgIcon>
  )
}

function TransitionComponent(props: TransitionProps) {
  const style = useSpring({
    to: {
      opacity: props.in ? 1 : 0,
      transform: `translate3d(${props.in ? 0 : 20}px,0,0)`
    }
  })

  return (
    <animated.div style={style}>
      <Collapse {...props} />
    </animated.div>
  )
}

const CustomContent = React.forwardRef(function CustomContent(
  props: TreeItemContentProps,
  ref
) {
  const {
    classes,
    className,
    label,
    nodeId,
    icon: iconProp,
    expansionIcon,
    displayIcon
  } = props

  const {
    disabled,
    expanded,
    selected,
    focused,
    handleExpansion,
    handleSelection,
    preventSelection
  } = useTreeItem(nodeId)

  const icon = iconProp || expansionIcon || displayIcon

  const handleMouseDown = (
    event: React.MouseEvent<HTMLDivElement, MouseEvent>
  ) => {
    preventSelection(event)
  }

  const handleExpansionClick = (
    event: React.MouseEvent<HTMLDivElement, MouseEvent>
  ) => {
    handleExpansion(event)
  }

  const handleSelectionClick = (
    event: React.MouseEvent<HTMLDivElement, MouseEvent>
  ) => {
    handleSelection(event)
  }

  return (
    <div
      className={clsx(className, classes.root, {
        [classes.expanded]: expanded,
        [classes.selected]: selected,
        [classes.focused]: focused,
        [classes.disabled]: disabled
      })}
      onMouseDown={handleMouseDown}
      ref={ref as React.Ref<HTMLDivElement>}
    >
      <div onClick={handleExpansionClick} className={classes.iconContainer}>
        {icon}
      </div>
      <Typography
        onClick={handleSelectionClick}
        component="div"
        className={classes.label}
      >
        {label}
      </Typography>
    </div>
  )
})

const CustomTreeItem = React.forwardRef(function CustomTreeItem(
  props: TreeItemProps,
  ref: React.Ref<HTMLLIElement>
) {
  return (
    <TreeItem
      {...props}
      ContentComponent={CustomContent}
      TransitionComponent={TransitionComponent}
      ref={ref}
    />
  )
})

const StyledTreeItem = styled(CustomTreeItem)(({ theme }) => ({
  [`& .${treeItemClasses.iconContainer}`]: {
    '& .close': {
      opacity: 0.3
    }
  },
  [`& .${treeItemClasses.group}`]: {
    marginLeft: 15,
    paddingLeft: 18,
    borderLeft: `1px dashed ${alpha(theme.palette.text.primary, 0.4)}`
  }
}))

export default function CustomizedTreeView({
  categories,
  isError,
  refetch,
  errorLabel,
  isLoading
}) {
  const {
    handleSubmit,
    control,
    reset,
    formState: { errors }
  } = useForm()

  const { t } = useTranslation('translation', {
    keyPrefix: 'page.Category'
  })
  const { t: v } = useTranslation('translation', {
    keyPrefix: 'validate'
  })

  const [expanded, setExpanded] = useState<string[]>([])
  const [openModalDelete, setOpenModalDelete] = useState(null)
  const [openModalEdit, setOpenModalEdit] = useState(null)

  const handleToggle = (event: React.SyntheticEvent, nodeIds: string[]) => {
    setExpanded(nodeIds)
  }

  const handleExpandClick = () => {
    setExpanded((oldExpanded) =>
      oldExpanded.length === 0 ? ['1', '5', '6', '7'] : []
    )
  }

  const convertedData = convertData(categories)

  const handleOpenModalDelete = (nodeId) => {
    setOpenModalDelete(nodeId)
  }
  const handleCloseModalDelete = () => {
    setOpenModalDelete(null)
  }

  const handleOpenModalEdit = (nodeId) => {
    setOpenModalEdit(nodeId)
  }
  const handleCloseModalEdit = () => {
    setOpenModalEdit(null)
    setSelectedImage(null)
    reset()
  }

  const [selectedImage, setSelectedImage] = useState(null)
  const handleImageChange = (e) => {
    const file = e.target.files[0]
    setSelectedImage(file)
  }

  //react-query: updateCategory
  const { mutate: mutateUpdate, isPending: isPendingUpdate } = useMutation({
    mutationFn: UpdateCategoryAPI,
    onSuccess: () => {
      toast.success('Cập nhật Category thành công')
      handleCloseModalEdit()
      refetch()
    },
    onError: (err) => {
      toastErrorMessage(err)
    }
  })
  const handleUpdateCategory = (nodeId) => async (data) => {
    data.image = selectedImage

    const formData = new FormData()
    formData.append('name', data.name)
    formData.append('category_id', nodeId)
    formData.append('image', data.image)
    if (data.parent_category_id && data.parent_category_id !== '0') {
      formData.append('parent_category_id', data.parent_category_id)
    } else if (data.parent_category_id === '0') {
      formData.append('parent_category_id', String(null))
    }

    mutateUpdate(formData)
  }

  //react-query: deleteCategory
  const { mutate: mutateDelete, isPending: isPendingDelete } = useMutation({
    mutationFn: DeleteCategoryAPI,
    onSuccess: () => {
      toast.success(`Xóa Category thành công`)
      handleCloseModalDelete()
      refetch()
    },
    onError: (err) => {
      toastErrorMessage(err)
    }
  })
  const handleDeleteClick = async (
    event: React.MouseEvent,
    categoryId: number
  ) => {
    event.stopPropagation()
    mutateDelete(categoryId)
  }

  const RenderTree = (nodes) => {
    return (
      <StyledTreeItem
        key={nodes.id}
        nodeId={nodes.id.toString()}
        label={
          <Box className="flex items-center justify-between py-[5px]">
            <Box className="flex items-center gap-[10px]">
              {nodes.image && (
                <img
                  src={nodes.image}
                  alt=""
                  width={50}
                  height={50}
                  className="rounded-full bg-[#E5E5E5]"
                />
              )}
              <Typography>{nodes.name}</Typography>
            </Box>

            <Box className="flex items-center justify-center gap-[5px]">
              <Edit
                fontSize="small"
                onClick={() => handleOpenModalEdit(nodes.id)}
              />
              <DeleteOutlinedIcon
                fontSize="small"
                onClick={() => handleOpenModalDelete(nodes.id)}
              />
            </Box>
            <Modal
              open={openModalEdit === nodes.id}
              onClose={handleCloseModalEdit}
            >
              <Stack spacing={4} sx={styleModal}>
                <Stack
                  spacing={5}
                  flex={1}
                  alignItems="center"
                  justifyContent="center"
                  p={4}
                  className="bg-white rounded-[15px] shadow-md"
                >
                  <label htmlFor="upload-photo">
                    <Stack
                      direction="row"
                      alignItems="center"
                      justifyContent="center"
                      className="border-[1px] bg-white border-gray-200 border-solid rounded-full w-[144px] h-[144px] hover:cursor-pointer"
                    >
                      <Box className="flex items-center justify-center border-[1px] border-gray-200 border-solid bg-gray-200 rounded-full w-[130px] h-[130px] hover:bg-gray-100">
                        <Stack
                          alignItems="center"
                          justifyContent="center"
                          spacing={2}
                        >
                          {selectedImage ? (
                            <img
                              src={URL.createObjectURL(selectedImage)}
                              alt="Selected"
                              width={130}
                              height={130}
                              className="rounded-full max-w-[130px] max-h-[130px]"
                            />
                          ) : nodes.image ? (
                            <>
                              <img
                                src={nodes.image}
                                alt="Selected"
                                width={130}
                                height={130}
                                className="rounded-full max-w-[130px] max-h-[130px]"
                              />
                            </>
                          ) : (
                            <>
                              <UploadIcon color="#919EAB" />
                              <Typography className="text-[#919EAB]">
                                {t('Upload Photo')}
                              </Typography>
                            </>
                          )}
                        </Stack>
                      </Box>
                    </Stack>
                  </label>
                  <Box className="text-center text-[#919EAB] max-w-[60%]">
                    <input
                      type="file"
                      id="upload-photo"
                      onChange={handleImageChange}
                    />
                    <Typography>
                      {t(
                        'Allowed *.jpeg, *.jpg, *.png, *.gif max size of 3 Mb'
                      )}
                    </Typography>
                  </Box>
                </Stack>
                <Controller
                  defaultValue={nodes.name}
                  name="name"
                  control={control}
                  rules={{
                    required: v('This field is required')
                  }}
                  render={({ field }) => (
                    <TextField
                      label={t('Category Name')}
                      error={!!errors.name}
                      helperText={errors.name?.message?.toString()}
                      {...field}
                    />
                  )}
                />

                <Controller
                  name="parent_category_id"
                  defaultValue={''}
                  control={control}
                  render={({ field }) => (
                    <FormControl>
                      <InputLabel id="demo-simple-select-label">
                        {t('Category Parent')}
                      </InputLabel>
                      <Select
                        id="demo-simple-select"
                        labelId="demo-simple-select-label"
                        label={t('Category Parent')}
                        defaultValue=""
                        {...field}
                      >
                        <MenuItem value="" disabled>
                          <Typography>{t('Select a category')}</Typography>
                        </MenuItem>
                        <MenuItem value="0">
                          <Typography>
                            {t('There is no parent category')}
                          </Typography>
                        </MenuItem>
                        {convertCategory(categories)
                          ?.filter((category) => category.id !== nodes.id)
                          .map((category) => (
                            <MenuItem
                              key={category.id}
                              value={category.id}
                              className="flex items-center gap-[10px]"
                            >
                              <Stack direction="row" alignItems="center">
                                <img
                                  width="30"
                                  height="30"
                                  src={category.image}
                                />
                                <Typography>{category.name}</Typography>
                              </Stack>
                            </MenuItem>
                          ))}
                      </Select>
                      <FormHelperText>
                        * {t('Leave blank if not updating the parent category')}
                      </FormHelperText>
                    </FormControl>
                  )}
                />

                <Stack direction="row" gap={3} justifyContent="end">
                  <Button
                    variant="contained"
                    color="error"
                    onClick={handleCloseModalEdit}
                  >
                    {t('Cancel')}
                  </Button>
                  <LoadingButton
                    variant="contained"
                    onClick={handleSubmit(handleUpdateCategory(nodes.id))}
                    loading={isPendingUpdate}
                  >
                    {t('Update Category')}
                  </LoadingButton>
                </Stack>
              </Stack>
            </Modal>
            <Modal
              open={openModalDelete === nodes.id}
              onClose={handleCloseModalDelete}
              aria-labelledby="modal-modal-title"
              aria-describedby="modal-modal-description"
            >
              <Box sx={styleModal}>
                <Typography className="flex flex-col items-center">
                  {t('Are you sure you want to delete Category')}
                  <Typography className="flex items-center">
                    <img
                      src={nodes.image}
                      className=" w-[30px] h-[30px] rounded-full bg-[#E5E5E5]"
                    />
                    <Typography variant="_emphasize">{nodes.name} </Typography>
                  </Typography>
                </Typography>
                <Box className="flex gap-[20px] pt-[15px] justify-center">
                  <Button
                    variant="contained"
                    color="error"
                    onClick={handleCloseModalDelete}
                  >
                    {t('Cancel')}
                  </Button>
                  <LoadingButton
                    variant="contained"
                    onClick={(event) => handleDeleteClick(event, nodes.id)}
                    loading={isPendingDelete}
                  >
                    {t('Confirm')}
                  </LoadingButton>
                </Box>
              </Box>
            </Modal>
          </Box>
        }
      >
        {Array.isArray(nodes.child_categories)
          ? nodes.child_categories.map((node) => RenderTree(node))
          : null}
      </StyledTreeItem>
    )
  }

  return (
    <Box sx={{ minHeight: 270, flexGrow: 1, maxWidth: 300 }}>
      <Button onClick={handleExpandClick}>
        {expanded.length === 0 ? t('Expand all') : t('Collapse all')}
      </Button>

      {isLoading || isError ? (
        <LoadingComponent
          loadingLabel={t('Loading category list')}
          isError={isError}
          errorLabel={errorLabel}
        />
      ) : (
        <TreeView
          aria-label="customized"
          defaultExpanded={['1']}
          defaultCollapseIcon={<MinusSquare />}
          defaultExpandIcon={<PlusSquare />}
          defaultEndIcon={<CloseSquare />}
          expanded={expanded}
          onNodeToggle={handleToggle}
          sx={{ overflowX: 'hidden' }}
        >
          {convertedData.map((node) => RenderTree(node))}
        </TreeView>
      )}
    </Box>
  )
}
