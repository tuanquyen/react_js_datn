import { MenuPropsSelectMulti } from '@/assets/styles/style'
import { Visibility } from '@mui/icons-material'
import {
  Box,
  Checkbox,
  FormControl,
  FormHelperText,
  InputLabel,
  MenuItem,
  OutlinedInput,
  Select,
  SelectChangeEvent,
  Stack,
  Tooltip,
  Typography
} from '@mui/material'
import { useEffect } from 'react'
import { useTranslation } from 'react-i18next'

export function ChooseTypeVoucher({
  valuesSelected,
  setValuesSelected,
  typeVoucher,
  setTypeVoucher,
  products,
  variants,
  categories
}) {
  const { t } = useTranslation('translation', { keyPrefix: 'page.Coupons' })
  const { t: v } = useTranslation('translation', { keyPrefix: 'validate' })

  const getLabelNameSelect = (labelName) => {
    switch (labelName) {
      case 'PRODUCT':
        return t('Select products')
      case 'CATEGORY':
        return t('Select categories')
      case 'VARIANT':
        return t('Select Variants')
      default:
        return ''
    }
  }

  const handleChange = (event: SelectChangeEvent<typeof valuesSelected>) => {
    const {
      target: { value }
    } = event
    setValuesSelected(
      // On autofill we get a stringified value.
      typeof value === 'string' ? value.split(',') : value
    )
  }

  useEffect(() => {
    setValuesSelected([])
  }, [typeVoucher])

  return (
    <Stack
      direction="row"
      spacing={4}
      sx={{
        alignItems: typeVoucher === 'ALL' ? 'center' : ''
      }}
    >
      <FormControl error={!typeVoucher} sx={{ flex: 1 }}>
        <InputLabel>{t('Voucher Type')}</InputLabel>
        <Select
          value={typeVoucher}
          label={t('Voucher Type')}
          onChange={(e: SelectChangeEvent) => setTypeVoucher(e.target.value)}
        >
          <MenuItem value="CATEGORY">
            <Typography>{t('Category')}</Typography>
          </MenuItem>
          <MenuItem value="PRODUCT">
            <Typography>{t('Product')}</Typography>
          </MenuItem>
          <MenuItem value="VARIANT">
            <Typography>{t('Variant')}</Typography>
          </MenuItem>
          <MenuItem value="ALL">
            <Typography>{t('All')}</Typography>
          </MenuItem>
        </Select>
        {typeVoucher ? (
          <></>
        ) : (
          <FormHelperText>{t('Please select voucher type')}</FormHelperText>
        )}
      </FormControl>

      {typeVoucher && typeVoucher !== 'ALL' ? (
        <FormControl error={!valuesSelected.length} sx={{ flex: 1 }}>
          <InputLabel id="demo-multiple-checkbox-label">
            {getLabelNameSelect(typeVoucher)}
          </InputLabel>
          <Select
            labelId="demo-multiple-checkbox-label"
            id="demo-multiple-checkbox"
            multiple
            value={valuesSelected}
            onChange={handleChange}
            input={<OutlinedInput label={getLabelNameSelect(typeVoucher)} />}
            renderValue={(selected) => selected.join(', ')}
            MenuProps={MenuPropsSelectMulti}
          >
            {typeVoucher === 'PRODUCT' ? (
              products.map((product) => (
                <MenuItem key={product.id} value={product.title}>
                  <Checkbox
                    size="small"
                    checked={valuesSelected.indexOf(product.title) > -1}
                  />
                  <Box className="flex items-center gap-[10px]">
                    <Tooltip title={product.name}>
                      <Visibility sx={{ fontSize: 14 }} />
                    </Tooltip>

                    <Box className="flex gap-[10px] items-center">
                      <img width="30" height="30" src={product?.image} alt="" />
                      <Typography>{product.name}</Typography>
                    </Box>
                  </Box>
                </MenuItem>
              ))
            ) : typeVoucher === 'CATEGORY' ? (
              categories.map((category) => (
                <MenuItem key={category.id} value={category.title}>
                  <Checkbox
                    size="small"
                    checked={valuesSelected.indexOf(category.title) > -1}
                  />
                  <Box className="flex items-center gap-[10px]">
                    <Tooltip title={category.name}>
                      <Visibility sx={{ fontSize: 14 }} />
                    </Tooltip>
                    <Box className="flex gap-[10px] items-center">
                      <img width="30" height="30" src={category.image} alt="" />
                      <Typography>{category.name}</Typography>
                    </Box>
                  </Box>
                </MenuItem>
              ))
            ) : typeVoucher === 'VARIANT' ? (
              variants.map((variant) => (
                <MenuItem key={variant.id} value={variant.title}>
                  <Checkbox
                    size="small"
                    checked={valuesSelected.indexOf(variant.title) > -1}
                  />
                  <Box className="flex items-center gap-[10px]">
                    <Tooltip title={variant.title}>
                      <Visibility sx={{ fontSize: 14 }} />
                    </Tooltip>
                    <Box className="flex gap-[10px] items-center">
                      <img width="30" height="30" src={variant.image} alt="" />
                      <Typography>{variant.title}</Typography>
                    </Box>
                  </Box>
                </MenuItem>
              ))
            ) : (
              <></>
            )}
          </Select>
          {valuesSelected.length === 0 ? (
            <FormHelperText>{v('This field is required')}</FormHelperText>
          ) : (
            <></>
          )}
        </FormControl>
      ) : (
        <></>
      )}
      {typeVoucher === 'ALL' && (
        <Typography className="text-gray-500">
          * Áp dụng cho tất cả các sản phẩm
        </Typography>
      )}
    </Stack>
  )
}
