'use client'
import React, { useEffect, useState } from 'react'
import { Box, Stack, TextField } from '@mui/material'
import { useForm, Controller } from 'react-hook-form'
import { convertArrayIds } from '@/utils/convertData'
import { DatePicker } from '@mui/x-date-pickers/DatePicker'
import dayjs, { Dayjs } from 'dayjs'
import { toast } from 'react-toastify'
import MainLayout from '@/components/MainLayout'
import { useNavigate } from 'react-router-dom'
import { toastErrorMessage } from '@/utils/errorMessage'
import AssignUserVoucher from '../AssignUserVoucher'
import { ChooseTypeVoucher } from '../ChooseTypeVoucher'
import { useMutation } from '@tanstack/react-query'
import { LoadingButton } from '@mui/lab'
import { CreateVoucherAPI } from '@/api/VoucherAPI'
import {
  AddTitleCategoriesLeftType,
  GetAllCategoryLeftAPI
} from '@/api/CategoryAPI'
import { AddTitleProductsType, GetAllProductAPI } from '@/api/ProductAPI'
import { GetAllVariantAPI } from '@/api/VariantColorAPI'
import { GetAllUserAPI } from '@/api/UserAPI'
import { useTranslation } from 'react-i18next'

function CreateVoucher() {
  const {
    control,
    handleSubmit,
    watch,
    formState: { errors }
  } = useForm()

  const maxPrice = watch('max_price')

  const navigate = useNavigate()
  const { t } = useTranslation('translation', { keyPrefix: 'page.Coupons' })
  const { t: v } = useTranslation('translation', { keyPrefix: 'validate' })

  const [valuesSelected, setValuesSelected] = useState<string[]>([])
  const [typeVoucher, setTypeVoucher] = useState<string>('')
  const { data: categories } = GetAllCategoryLeftAPI()
  const customCategories: AddTitleCategoriesLeftType[] | undefined =
    categories?.map((category) => ({
      id: category.id,
      name: category.name,
      image: category.image,
      title: `${category.name}(${category.id})`
    }))

  const { filteredProducts: products, mutation } = GetAllProductAPI()
  useEffect(() => {
    mutation.mutate({ sort_by: 'Mới nhất' })
  }, [])
  const customProducts: AddTitleProductsType[] | undefined = products?.map(
    (product) => ({
      id: product.id,
      name: product.name,
      image: product.image,
      title: `${product.name}(${product.id})`
    })
  )

  const { data: variants } = GetAllVariantAPI()
  const customVariants = variants?.map((variant) => ({
    id: variant.id,
    name: '',
    image: variant?.product?.product_image[0]?.image,
    title: `(${variant.size} - ${variant.color.name}) ${variant.product.name}`
  }))

  const { data: users } = GetAllUserAPI()
  const customUsers = users?.map((user) => ({
    id: user.id,
    name: user.name,
    email: user.email,
    image: user.image,
    phone_number: user.phone_number,
    title: `${user.name} (${user.id}) - ${user.email} - ${user.phone_number}`
  }))

  const [value1, setValue1] = useState<Dayjs | null>(dayjs())
  const [value2, setValue2] = useState<Dayjs | null>(dayjs())

  const [usersSelect, setUsersSelect] = useState<string[]>([])

  //react-query: createVoucher
  const { mutate: mutateCreateVoucher, isPending: isPendingCreateVoucher } =
    useMutation({
      mutationFn: CreateVoucherAPI,
      onSuccess: () => {
        toast.success('Tạo voucher thành công')
        navigate('/catalogs/coupons')
      },
      onError: (err) => {
        toastErrorMessage(err)
      }
    })
  const handleCreateVoucher = async (data) => {
    if (typeVoucher === 'CATEGORY') {
      data.categories_id = convertArrayIds(customCategories, valuesSelected)
    } else if (typeVoucher === 'PRODUCT') {
      data.products_id = convertArrayIds(customProducts, valuesSelected)
    } else if (typeVoucher === 'VARIANT') {
      data.variants_id = convertArrayIds(customVariants, valuesSelected)
    }
    data.type = typeVoucher
    data.started_at = value1?.unix()
    data.expired_at = value2?.unix()
    data.remaining_quantity = Number(data.remaining_quantity)
    data.max_price = Number(data.max_price)
    data.min_price_apply = Number(data.min_price_apply)
    data.percent_discount = Number(data.percent_discount)
    if (usersSelect.length > 0) {
      data.users_id = convertArrayIds(customUsers, usersSelect)
    }
    mutateCreateVoucher(data)
  }

  return (
    <Box className="flex ">
      <Stack
        spacing={5}
        width="90%"
        sx={{ background: (theme) => theme.palette._white_212b36.main }}
        p={10}
      >
        <Stack>
          <ChooseTypeVoucher
            valuesSelected={valuesSelected}
            setValuesSelected={setValuesSelected}
            typeVoucher={typeVoucher}
            setTypeVoucher={setTypeVoucher}
            products={customProducts}
            variants={customVariants}
            categories={customCategories}
          />
        </Stack>

        <Stack>
          <Controller
            defaultValue=""
            name="name"
            control={control}
            rules={{ required: v('This field is required') }}
            render={({ field }) => (
              <TextField
                label={t('Voucher Name')}
                error={!!errors.name}
                helperText={errors.name?.message?.toString()}
                {...field}
              />
            )}
          />
        </Stack>
        <Stack direction="row" spacing={4}>
          <DatePicker
            sx={{ flex: 1 }}
            label={t('Start Date')}
            value={value1}
            onChange={(newValue) => setValue1(newValue)}
          />
          <DatePicker
            sx={{ flex: 1 }}
            label={t('Expire Date')}
            value={value2}
            onChange={(newValue) => setValue2(newValue)}
          />
        </Stack>
        <Stack direction="row" spacing={4}>
          <Controller
            name="remaining_quantity"
            control={control}
            rules={{
              required: v('This field is required'),
              min: { value: 1, message: v('Minimum value is 1') }
            }}
            render={({ field }) => (
              <TextField
                sx={{ flex: 1 }}
                type="number"
                label={t('Quantity')}
                error={!!errors.remaining_quantity}
                helperText={errors.remaining_quantity?.message?.toString()}
                {...field}
              />
            )}
          />
          <Controller
            name="max_price"
            control={control}
            rules={{
              required: v('This field is required'),
              min: { value: 0, message: v('Invalid value') }
            }}
            render={({ field }) => (
              <TextField
                sx={{ flex: 1 }}
                type="number"
                label={t('Max Price')}
                error={!!errors.max_price}
                helperText={errors.max_price?.message?.toString()}
                {...field}
              />
            )}
          />
        </Stack>
        <Stack direction="row" spacing={4}>
          <Controller
            name="min_price_apply"
            control={control}
            rules={{
              required: v('This field is required'),
              min: { value: 0, message: v('Invalid value') },
              validate: (value) =>
                parseFloat(value) > parseFloat(maxPrice) ||
                v('Min price apply must be greater than max price')
            }}
            render={({ field }) => (
              <TextField
                sx={{ flex: 1 }}
                type="number"
                label={t('Min Price Apply')}
                error={!!errors.min_price_apply}
                helperText={errors.min_price_apply?.message?.toString()}
                {...field}
              />
            )}
          />
          <Controller
            name="percent_discount"
            control={control}
            rules={{
              required: v('This field is required'),
              min: { value: 1, message: v('The value ranges from 1 to 100') },
              max: { value: 100, message: v('The value ranges from 1 to 100') }
            }}
            render={({ field }) => (
              <TextField
                sx={{ flex: 1 }}
                type="number"
                label={t('Percent Discount')}
                error={!!errors.percent_discount}
                helperText={errors.percent_discount?.message?.toString()}
                {...field}
              />
            )}
          />
        </Stack>

        <Box className="flex gap-[15px]">
          <AssignUserVoucher
            usersSelect={usersSelect}
            setUsersSelect={setUsersSelect}
            users={customUsers}
          />
        </Box>

        <Box>
          <LoadingButton
            variant="contained"
            onClick={handleSubmit(handleCreateVoucher)}
            loading={isPendingCreateVoucher}
          >
            {t('Create Voucher')}
          </LoadingButton>
        </Box>
      </Stack>
    </Box>
  )
}

export default function CreateVoucherPage() {
  const { t } = useTranslation('translation', { keyPrefix: 'page.Coupons' })
  return (
    <MainLayout
      title={t('Create Voucher')}
      back={true}
      content={<CreateVoucher />}
    />
  )
}
