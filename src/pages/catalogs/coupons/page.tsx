import React from 'react'
import { Box, Button, Stack, Typography } from '@mui/material'
import { useNavigate } from 'react-router-dom'
import VoucherTable from './VoucherTable'
import { GetAllVoucherAPI } from '@/api/VoucherAPI'
import { useTranslation } from 'react-i18next'

export default function CouponsPage() {
  const navigate = useNavigate()
  const { t } = useTranslation('translation', { keyPrefix: 'page.Coupons' })

  const {
    data: vouchers,
    isLoading,
    isError,
    error,
    refetch
  } = GetAllVoucherAPI()

  return (
    <Stack spacing={5}>
      <Typography variant="_title">{t('Coupons Page')}</Typography>
      <Box>
        <Button
          variant="contained"
          className="!bg-primary"
          onClick={() => {
            navigate('/createVoucher')
          }}
        >
          {t('Add Voucher')}
        </Button>
      </Box>
      <VoucherTable
        vouchers={vouchers}
        refetch={refetch}
        isLoading={isLoading}
        isError={isError}
        errorLabel={error?.message}
      />
    </Stack>
  )
}
