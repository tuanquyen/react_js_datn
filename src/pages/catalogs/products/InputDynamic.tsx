import { useState } from 'react'
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import DeleteOutlineOutlined from '@mui/icons-material/DeleteOutlineOutlined'

//Tham khảo component tại https://github.com/chaoocharles/add-remove-form-field/blob/main/src/App.js
function InputDynamic({ data, setData, dataChild, buttonName }) {
  const [error, setError] = useState<boolean>(false)
  const handleServiceChange = (
    e: React.ChangeEvent<HTMLInputElement>,
    index: number
  ) => {
    setError(false)
    const { value } = e.target
    setData((prevData) => ({
      ...prevData,
      [dataChild]: prevData[dataChild].map((item, i) =>
        i === index ? value : item
      )
    }))
  }

  const handleServiceRemove = (index: number) => {
    setError(false)
    setData((prevData) => {
      const updatedPattern = [...prevData[dataChild]]
      updatedPattern.splice(index, 1)
      return {
        ...prevData,
        [dataChild]: updatedPattern
      }
    })
  }

  const handleServiceAdd = () => {
    const isAnyInputEmpty = data[dataChild].some((item) => item === '')
    if (!isAnyInputEmpty) {
      setData((prevData) => ({
        ...prevData,
        [dataChild]: [...prevData[dataChild], '']
      }))
    } else {
      // Xử lý khi có thẻ input còn trống
      setError(true)
    }
  }

  return (
    <Box>
      <Box>
        {data[dataChild].map((patternItem, index) => (
          <Box key={index} className="flex flex-col gap-[10px]">
            <Box className="flex items-center w-[100%]">
              <input
                name="service"
                type="text"
                id="service"
                value={patternItem}
                onChange={(e) => handleServiceChange(e, index)}
                required
                className={`input ${
                  error &&
                  index === data[dataChild].length - 1 &&
                  '!border-1 !border-red-500'
                }`}
              />
              {data[dataChild].length !== 1 && (
                <DeleteOutlineOutlined
                  onClick={() => handleServiceRemove(index)}
                  sx={{
                    color: '#e53935',
                    '&:hover': {
                      cursor: 'pointer'
                    }
                  }}
                />
              )}
            </Box>
            {error && index === data[dataChild].length - 1 && (
              <p className="error">This field is required</p>
            )}

            <Box>
              {data[dataChild].length - 1 === index && (
                <Button variant="outlined" onClick={handleServiceAdd}>
                  {buttonName}
                </Button>
              )}
            </Box>
          </Box>
        ))}
      </Box>
    </Box>
  )
}

export default InputDynamic
