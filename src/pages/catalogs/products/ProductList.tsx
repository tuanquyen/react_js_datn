import React, { useEffect } from 'react'
import Stack from '@mui/material/Stack'
import Grid from '@mui/material/Grid'
import Box from '@mui/material/Box'
// import { randomId } from '@mui/x-data-grid-generator'
import ShopProductCard from './product-card'
import ProductFilter from './ProductFilter'
import { Typography } from '@mui/material'
import { GetAllProductAPI } from '@/api/ProductAPI'

// const PRODUCT_NAME = [
//   'Nike Air Force 1 NDESTRUKT',
//   'Nike Space Hippie 04',
//   'Nike Air Zoom Pegasus 37 A.I.R. Chaz Bear',
//   'Nike Blazer Low 77 Vintage',
//   'Nike ZoomX SuperRep Surge',
//   'Zoom Freak 2',
//   'Nike Air Max Zephyr',
//   'Jordan Delta',
//   'Air Jordan XXXV PF',
//   'Nike Waffle Racer Crater',
//   'Kyrie 7 EP Sisterhood',
//   'Nike Air Zoom BB NXT',
//   'Nike Air Force 1 07 LX',
//   'Nike Air Force 1 Shadow SE',
//   'Nike Air Zoom Tempo NEXT%',
//   'Nike DBreak-Type',
//   'Nike Air Max Up',
//   'Nike Air Max 270 React ENG',
//   'NikeCourt Royale',
//   'Nike Air Zoom Pegasus 37 Premium',
//   'Nike Air Zoom SuperRep',
//   'NikeCourt Royale',
//   'Nike React Art3mis',
//   'Nike React Infinity Run Flyknit A.I.R. Chaz Bear'
// ]
// const PRODUCT_COLOR = [
//   '#00AB55',
//   '#000000',
//   '#FFFFFF',
//   '#FFC0CB',
//   '#FF4842',
//   '#1890FF',
//   '#94D82D',
//   '#FFC107'
// ]

// ----------------------------------------------------------------------

// const products = [...Array(24)].map((_, index) => {
//   const setIndex = index + 1

//   return {
//     id: randomId(),
//     cover: `https://dotilo.com/image/catalog/coupon/aotron/dodo.jpg`,
//     name: PRODUCT_NAME[index],
//     price: 76,
//     priceSale: 70,
//     colors:
//       (setIndex === 1 && PRODUCT_COLOR.slice(0, 2)) ||
//       (setIndex === 2 && PRODUCT_COLOR.slice(1, 3)) ||
//       (setIndex === 3 && PRODUCT_COLOR.slice(2, 4)) ||
//       (setIndex === 4 && PRODUCT_COLOR.slice(3, 6)) ||
//       (setIndex === 23 && PRODUCT_COLOR.slice(4, 6)) ||
//       (setIndex === 24 && PRODUCT_COLOR.slice(5, 6)) ||
//       PRODUCT_COLOR,
//     status: 'sale'
//   }
// })

export default function ProductList() {
  const { filteredProducts, mutation } = GetAllProductAPI()

  useEffect(() => {
    mutation.mutate({ sort_by: 'Mới nhất' })
  }, [])

  console.log(filteredProducts)

  return (
    <Box>
      <Stack direction="row" spacing={2} mt={2} mb={5}>
        <ProductFilter mutation={mutation} />
      </Stack>
      {filteredProducts.length > 0 ? (
        <Grid container className="gap-[15px]">
          {filteredProducts.map((product) => (
            <Grid item key={product.id} xs={12} sm={6} md={2.8}>
              <ShopProductCard product={product} />
            </Grid>
          ))}
        </Grid>
      ) : (
        <Box className="flex justify-center items-center  min-h-[200px]">
          <Typography variant="_emphasize">No Product</Typography>
        </Box>
      )}
    </Box>
  )
}
