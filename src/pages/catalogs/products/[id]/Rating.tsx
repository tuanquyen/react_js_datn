import { useState } from 'react'
import Rating from '@mui/material/Rating'
import Box from '@mui/material/Box'
import StarIcon from '@mui/icons-material/Star'

const labels: { [index: string]: string } = {
  1: 'Tệ',

  2: 'Không hài lòng',

  3: 'Bình thường',

  4: 'Hài lòng',

  5: 'Tuyệt vời'
}

function getLabelText(value: number) {
  return `${value} Star${value !== 1 ? 's' : ''}, ${labels[value]}`
}

export default function CreateRating() {
  const [value, setValue] = useState<number | null>(5)
  const [hover, setHover] = useState(-1)

  return (
    <Box
      sx={{
        width: 200,
        display: 'flex',
        alignItems: 'center'
      }}
    >
      <Rating
        name="hover-feedback"
        value={value}
        precision={1}
        getLabelText={getLabelText}
        onChange={(event, newValue) => {
          setValue(newValue)
        }}
        onChangeActive={(event, newHover) => {
          setHover(newHover)
        }}
        emptyIcon={<StarIcon style={{ opacity: 0.55 }} fontSize="inherit" />}
      />
      {value !== null && (
        <Box sx={{ ml: 2, minWidth: '200px' }}>
          {labels[hover !== -1 ? hover : value]}
        </Box>
      )}
    </Box>
  )
}
