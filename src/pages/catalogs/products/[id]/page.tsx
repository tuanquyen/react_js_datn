import 'yet-another-react-lightbox/styles.css'
import LightBoxLibrary from './LightBoxLibrary'
import TabProductDetail from './TabProductDetail'
import { useParams, useNavigate } from 'react-router-dom'
import MainLayout from '@/components/MainLayout'
import { toast } from 'react-toastify'
import { useState } from 'react'
import { formatDateTime } from '@/utils/format'
import { Button, Rating, Box, Typography, Grid, Stack } from '@mui/material'
import { ConvertIdToName } from '@/utils/convertData'
import { toastErrorMessage } from '@/utils/errorMessage'
import { useMutation } from '@tanstack/react-query'
import { LoadingButton } from '@mui/lab'
import ModalMUI from '@/components/ModalMUI'
import LoadingComponent from '@/components/loading'
import { GetAllCategoryLeftAPI } from '@/api/CategoryAPI'
import { DeleteProductAPI, GetProductDetailAPI } from '@/api/ProductAPI'
import { useTranslation } from 'react-i18next'

const Child = () => {
  const { id: product_id } = useParams()
  const navigate = useNavigate()

  const { t } = useTranslation('translation', { keyPrefix: 'page.Product' })

  const [openModalDeleteProduct, setOpenModalDeleteProduct] =
    useState<boolean>(false)
  const handleOpenModalDeleteProduct = () => setOpenModalDeleteProduct(true)
  const handleCloseModalDeleteProduct = () => {
    setOpenModalDeleteProduct(false)
  }

  const { data: categoryLeafList } = GetAllCategoryLeftAPI()

  const {
    data: productData,
    refetch: refetchProductData,
    isLoading,
    isError,
    error
  } = GetProductDetailAPI(Number(product_id))

  const transformedArray = productData?.product_image.map((imageObj) => ({
    src: imageObj.image
  }))

  //react-query: deleteProduct
  const { mutate: mutateDelete, isPending: isPendingDelete } = useMutation({
    mutationFn: DeleteProductAPI,
    onSuccess: () => {
      toast.success('Xóa sản phẩm thành công')
      navigate('/catalogs/products')
    },
    onError: (err) => {
      toastErrorMessage(err)
      handleCloseModalDeleteProduct()
    }
  })
  const handleDeleteProduct = async () => {
    mutateDelete(product_id)
  }

  if (isLoading || isError) {
    return (
      <LoadingComponent
        loadingLabel="Get Product Detail"
        isError={isError}
        errorLabel={error?.message}
      />
    )
  }

  return (
    <Stack spacing={5}>
      <Stack direction="row" alignItems="center" justifyContent="flex-end">
        <Stack
          direction="row"
          alignItems="center"
          justifyContent="center"
          spacing={5}
        >
          <Button
            variant="contained"
            onClick={() => navigate(`/updateProduct/${product_id}`)}
          >
            {t('Update Product')}
          </Button>
          <Button
            variant="contained"
            onClick={handleOpenModalDeleteProduct}
            color="error"
          >
            {t('Delete Product')}
          </Button>
        </Stack>
        <ModalMUI
          open={openModalDeleteProduct}
          onClose={handleCloseModalDeleteProduct}
          minWidth={500}
        >
          <Stack spacing={4} alignItems="center">
            <Typography>
              {t('Are you sure you want to delete Product')}
              <Typography variant="_emphasize">
                {productData?.name}
              </Typography>{' '}
            </Typography>
            <Stack direction="row" justifyContent="center" spacing={4}>
              <Button
                variant="contained"
                color="error"
                onClick={handleCloseModalDeleteProduct}
              >
                {t('Cancel')}
              </Button>
              <LoadingButton
                variant="contained"
                onClick={handleDeleteProduct}
                loading={isPendingDelete}
              >
                {t('Confirm')}
              </LoadingButton>
            </Stack>
          </Stack>
        </ModalMUI>
      </Stack>

      <Stack direction="row">
        <LightBoxLibrary slides={transformedArray} />
        <Stack spacing={3} my={2} flex={3}>
          <Typography variant="_title">
            {t('Product Name')}: {productData?.name}
          </Typography>
          <Rating
            name="read-only"
            value={Number(productData?.avg_rating)}
            readOnly
          />
          <Box className="border-b border-[1px] border-dashed border-gray-300"></Box>

          <Grid container rowSpacing={3} className="text-gray-500">
            <Grid item xs={6}>
              <Typography>{t('Sold')}</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography>{productData?.count_bought}</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography className="w-[220px]">{t('Category')}</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography>
                {ConvertIdToName(categoryLeafList, productData?.category_id)}
              </Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography>{t('Manufacturer')}</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography>{productData?.manufacturer}</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography> {t('Material')} </Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography> {productData?.material} </Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography> {t('Create at')} </Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography>{formatDateTime(productData?.created_at)}</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography> {t('Pattern')} </Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography> {productData?.pattern} </Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography> {t('Style')} </Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography> {productData?.style} </Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography> {t('Other attributes')} </Typography>
            </Grid>
            <Grid item xs={6}></Grid>
            <Grid item xs={12}>
              {productData?.others?.map((other) => (
                <li key={other.name}>
                  <Typography>
                    {other.name}: {other.value}
                  </Typography>
                </li>
              ))}
            </Grid>
          </Grid>
        </Stack>
      </Stack>
      <Box>
        <TabProductDetail
          variants={productData?.variants}
          product_id={productData?.id}
          refetch={refetchProductData}
          countReview={productData?.count_review}
          productImage={productData?.product_image}
        />
      </Box>
    </Stack>
  )
}

export default function ProductDetail() {
  return <MainLayout back={true} content={<Child />} />
}
