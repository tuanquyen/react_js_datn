import MainLayout from '@/components/MainLayout'
import { useState } from 'react'
import { Controller, useForm } from 'react-hook-form'
import DoubleInputDynamic from '../DoubleInputDynamic'
import { toast } from 'react-toastify'
import AddVariantFix from '../AddVariantFix'
import { toastErrorMessage } from '@/utils/errorMessage'
import {
  FormControl,
  FormHelperText,
  InputLabel,
  MenuItem,
  Select,
  TextField,
  Box,
  Button,
  Stack,
  Typography,
  useTheme
} from '@mui/material'
import { useMutation } from '@tanstack/react-query'
import { GetAllCategoryLeftAPI } from '@/api/CategoryAPI'
import { CreateProductAPI, CreateProductAPIBodyType } from '@/api/ProductAPI'
import { useTranslation } from 'react-i18next'

const AddProduct = () => {
  //react-hook-form: validate chỉ cho name và manufacturer
  const {
    handleSubmit,
    control,
    formState: { errors }
  } = useForm<CreateProductAPIBodyType>()

  const { t } = useTranslation('translation', { keyPrefix: 'page.Product' })
  const { t: v } = useTranslation('translation', { keyPrefix: 'validate' })

  //Dữ liệu form (Add Product)
  const [othersProduct, setOthersProduct] = useState([])

  const [openVariant, setOpenVariant] = useState(false)
  const [productIdVariant, setProductIdVariant] = useState('')

  const { data: categoryLeafList } = GetAllCategoryLeftAPI()

  const theme = useTheme()

  const { mutate: mutateCreate, isPending: isPendingCreate } = useMutation({
    mutationFn: CreateProductAPI,
    onSuccess: (res) => {
      toast.success('Thêm sản phẩm thành công')
      setOpenVariant(true)
      setProductIdVariant(res.data.data.product_id)
    },
    onError: (err) => {
      toastErrorMessage(err)
    }
  })

  //Submit: Connect BE to send AddProduct data
  const handleAddProduct = async (data: CreateProductAPIBodyType) => {
    data.category_id = Number(data.category_id)
    if (othersProduct.length > 0) {
      data.others = othersProduct
    }
    mutateCreate(data)
  }

  return (
    <Box>
      <Stack>
        <Box
          maxWidth={600}
          sx={{
            backgroundColor: 'transparent',
            [theme.breakpoints.up('md')]: {
              backgroundColor: theme.palette._white_212b36.main
            },
            paddingX: { xs: 0, md: 3, lg: 6 },
            paddingY: { xs: 0, md: 3, lg: 10 }
          }}
        >
          <Stack spacing={5}>
            <Controller
              name="name"
              control={control}
              rules={{
                required: v('This field is required')
              }}
              render={({ field }) => (
                <TextField
                  label={t('Product Name')}
                  error={!!errors.name}
                  helperText={errors.name?.message?.toString()}
                  {...field}
                />
              )}
            />
            <Controller
              name="manufacturer"
              control={control}
              render={({ field }) => (
                <TextField
                  label={t('Manufacturer')}
                  error={!!errors.manufacturer}
                  helperText={errors.manufacturer?.message?.toString()}
                  {...field}
                />
              )}
            />

            <Controller
              name="pattern"
              control={control}
              render={({ field }) => (
                <TextField
                  label={t('Pattern')}
                  error={!!errors.pattern}
                  helperText={errors.pattern?.message?.toString()}
                  {...field}
                />
              )}
            />
            <Controller
              name="material"
              control={control}
              render={({ field }) => (
                <TextField
                  label={t('Material')}
                  error={!!errors.material}
                  helperText={errors.material?.message?.toString()}
                  {...field}
                />
              )}
            />

            <Controller
              name="style"
              control={control}
              render={({ field }) => (
                <TextField
                  label={t('Style')}
                  error={!!errors.style}
                  helperText={errors.style?.message?.toString()}
                  {...field}
                />
              )}
            />

            <Stack spacing={3}>
              <Typography variant="_emphasize">
                {t('Other attributes')}
              </Typography>
              <Stack spacing={5}>
                <DoubleInputDynamic
                  data={othersProduct}
                  setData={setOthersProduct}
                  buttonName={t('Add attribute')}
                />
              </Stack>
            </Stack>

            <Controller
              defaultValue={undefined}
              name="category_id"
              control={control}
              rules={{
                required: v('This field is required')
              }}
              render={({ field }) => (
                <FormControl error={!!errors.category_id}>
                  <InputLabel id="demo-simple-select-label">
                    {t('Category')}
                  </InputLabel>
                  <Select
                    id="demo-simple-select"
                    labelId="demo-simple-select-label"
                    label={t('Category')}
                    {...field}
                  >
                    <MenuItem value="" disabled>
                      <Typography>{t('Select a category')}</Typography>
                    </MenuItem>

                    {categoryLeafList?.map((category) => (
                      <MenuItem key={category.id} value={category.id}>
                        <Stack direction="row" spacing={2} alignItems="center">
                          <img
                            width="30"
                            height="30"
                            src={category.image || ''}
                          />
                          <Typography> {category.name}</Typography>
                        </Stack>
                      </MenuItem>
                    ))}
                  </Select>
                  <FormHelperText>
                    {errors.category_id?.message?.toString()}
                  </FormHelperText>
                </FormControl>
              )}
            />
          </Stack>
          <Stack spacing={5} pt={5}>
            {openVariant && (
              <Box>
                <AddVariantFix productId={productIdVariant} />
              </Box>
            )}
            <Box>
              {openVariant === false ? (
                <Button
                  variant="contained"
                  onClick={handleSubmit(handleAddProduct)}
                  disabled={isPendingCreate}
                >
                  {t('Create Product')}
                </Button>
              ) : (
                <></>
              )}
            </Box>
          </Stack>
        </Box>
        {/* End Body Modal */}
      </Stack>
    </Box>
  )
}

export default function CreateProductPage() {
  const { t } = useTranslation('translation', { keyPrefix: 'page.Product' })
  return (
    <MainLayout
      title={t('Create Product')}
      content={<AddProduct />}
      back={true}
    />
  )
}
