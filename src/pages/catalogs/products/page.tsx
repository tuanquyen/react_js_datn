import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import ProductTable from './ProductTable'
import { Box, Tab, Button, Typography } from '@mui/material'
import { TabList, TabContext, TabPanel } from '@mui/lab'
import ProductList from './ProductList'
import { Add } from '@mui/icons-material'
import { useTranslation } from 'react-i18next'

const ProductLayout = () => {
  const navigate = useNavigate()

  const { t } = useTranslation('translation', { keyPrefix: 'page.Product' })

  //State của Tab
  const [value, setValue] = useState(() => {
    return localStorage.getItem('currentTabProduct') || '1'
  })
  const handleChange = (event: React.SyntheticEvent, newValue: string) => {
    setValue(newValue)
  }
  useEffect(() => {
    localStorage.setItem('currentTabProduct', value)
  }, [value])

  return (
    <Box className="flex flex-col gap-[20px]">
      <Typography variant="_title">{t('Product Page')}</Typography>
      <Box
        className="px-[15px] py-[30px] rounded-lg"
        sx={{ background: (theme) => theme.palette._white_212b36.main }}
      >
        <Button
          variant="contained"
          startIcon={<Add />}
          onClick={() => navigate('/createProduct')}
          color="secondary"
        >
          {t('Add Product')}
        </Button>
      </Box>

      <Box
        sx={{
          width: '100%',
          typography: 'body1',
          borderRadius: '6px',
          background: (theme) => theme.palette._white_212b36.main
        }}
      >
        <TabContext value={value}>
          <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
            <TabList onChange={handleChange} aria-label="lab API tabs example">
              <Tab label={t('Table')} value="1" />
              <Tab label={t('List')} value="2" />
            </TabList>
          </Box>
          <TabPanel value="1">
            <ProductTable productList={[]} />
          </TabPanel>
          <TabPanel value="2">
            <ProductList />
          </TabPanel>
        </TabContext>
      </Box>
    </Box>
  )
}

export default function ProductPage() {
  return <ProductLayout />
}
