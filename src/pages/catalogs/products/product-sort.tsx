import { useState } from 'react'
import Button from '@mui/material/Button'
import MenuItem from '@mui/material/MenuItem'
import FormControl from '@mui/material/FormControl'
import Select, { SelectChangeEvent } from '@mui/material/Select'
// ----------------------------------------------------------------------

const SORT_OPTIONS = [
  { value: 'featured', label: 'Featured' },
  { value: 'newest', label: 'Newest' },
  { value: 'priceDesc', label: 'Price: High-Low' },
  { value: 'priceAsc', label: 'Price: Low-High' }
]

export default function ShopProductSort() {
  const [filter, setFilter] = useState('')

  const handleChange = (event: SelectChangeEvent) => {
    setFilter(event.target.value)
  }

  return (
    <>
      <Button disableRipple color="inherit" className="font-bold">
        Sort By:&nbsp;
        <FormControl
          sx={{
            minWidth: 120,
            '& .MuiInputBase-root': {
              height: 'fit-content'
            },
            '& .MuiSelect-select': {
              p: 0
            },
            '& fieldset': {
              border: 'none'
            }
          }}
          size="small"
        >
          <Select value={filter || 'Select'} onChange={handleChange}>
            {SORT_OPTIONS.map((item) => (
              <MenuItem key={item.value} value={item.value}>
                {item.label}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </Button>
    </>
  )
}
