import React from 'react'
import { Stack, Typography } from '@mui/material'

interface AmountBoxProps {
  icon: React.ReactNode
  iconColor: string
  bgicon: string
  title: string
  amount: string | number
}

export default function AmountBox({
  icon,
  iconColor,
  bgicon,
  title,
  amount
}: AmountBoxProps) {
  return (
    <Stack
      direction="row"
      spacing={2}
      p={4}
      alignItems="center"
      justifyContent="start"
      className="border border-solid border-[#ccc] rounded-[8px]"
      width={{ xs: '100%', sm: 300, md: 250 }}
    >
      <Stack
        sx={{
          backgroundColor: `${bgicon}`,
          color: `${iconColor}`
        }}
        className="p-[5px] rounded-full"
      >
        {icon}
      </Stack>
      <Stack spacing={1}>
        <Typography>{title}</Typography>
        <Typography className="font-bold">
          {amount.toLocaleString('it-IT', {
            style: 'currency',
            currency: 'VND'
          })}
        </Typography>
      </Stack>
    </Stack>
  )
}
