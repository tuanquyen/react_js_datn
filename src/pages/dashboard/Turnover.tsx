import React from 'react'
import Box from '@mui/material/Box'

interface TurnoverProps {
  icon: React.ReactNode
  title: string
  money: string
  cash: string
  card: string
  credit: string
  bgcolor: string
}

export default function Turnover({
  icon,
  title,
  money,
  cash,
  card,
  credit,
  bgcolor
}: TurnoverProps) {
  return (
    <Box
      sx={{ backgroundColor: `${bgcolor}` }}
      className="px-[15px] py-[25px] text-[#fff] flex flex-col items-center gap-[20px] max-w-[200px] rounded-[8px] flex-1"
    >
      <Box component="div">{icon}</Box>
      <Box component="div" className="text-[16px]">
        {title}
      </Box>
      <Box component="div" className="text-[24px] font-bold">
        {money}
      </Box>
      <Box
        component="div"
        className="flex justify-center items-center gap-[12px] text-[12.5px] "
      >
        <Box component="div" className="flex flex-col gap-[3px]">
          <Box component="div">Cash</Box>
          <Box component="div">{cash}</Box>
        </Box>
        <Box component="div" className="flex flex-col gap-[3px]">
          <Box component="div">Card</Box>
          <Box component="div">{card}</Box>
        </Box>
        <Box component="div" className="flex flex-col gap-[3px]">
          <Box component="div">Credit</Box>
          <Box component="div">{credit}</Box>
        </Box>
      </Box>
    </Box>
  )
}
