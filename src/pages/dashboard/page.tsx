import React from 'react'
import Box from '@mui/material/Box'
import AmountBox from './AmountBox'
import AddShoppingCartOutlined from '@mui/icons-material/AddShoppingCartOutlined'
import DoughnutChart from './charts/DoughnutChart'
import SimpleCard from './charts/SimpleCard'
import LineChart from './charts/LineChart'
import BarCharts from './BarColumns'
import { BestSellingTable } from './BestSellingTable'
import { Stack, Typography } from '@mui/material'
import { useTranslation } from 'react-i18next'
import { GetOverViewDashboardAPI } from '@/api/DashboardAPI'

export default function DashboardPage() {
  const { t } = useTranslation('translation', {
    keyPrefix: 'page.Dashboard'
  })
  const { data: result } = GetOverViewDashboardAPI()

  return (
    <Stack gap={{ xs: 5, lg: 8 }}>
      <Typography variant="_title">{t('Dashboard Page')}</Typography>
      <Stack direction="row" sx={{ flexWrap: 'wrap', gap: 5 }}>
        <AmountBox
          icon={<AddShoppingCartOutlined />}
          title={t('Orders Waiting')}
          amount={Number(result?.waiting?.value)}
          bgicon="#FFEDD5"
          iconColor="#EA580C"
        />
        <AmountBox
          icon={<AddShoppingCartOutlined />}
          title={t('Orders Confirm')}
          amount={Number(result?.confirm?.value)}
          bgicon="#DBEAFE"
          iconColor="#2563EB"
        />
        <AmountBox
          icon={<AddShoppingCartOutlined />}
          title={t('Orders Shipping')}
          amount={Number(result?.shipping?.value)}
          bgicon="#CCFBF1"
          iconColor="#0D9488"
        />
        <AmountBox
          icon={<AddShoppingCartOutlined />}
          title={t('Orders Done')}
          amount={Number(result?.done?.value)}
          bgicon="#D1FAE5"
          iconColor="#059669"
        />
        <AmountBox
          icon={<AddShoppingCartOutlined />}
          title={t('Orders Cancel')}
          amount={Number(result?.cancel?.value)}
          bgicon="#D1FAE5"
          iconColor="#059669"
        />
      </Stack>
      <Box
        className="flex gap-8"
        sx={{ flexDirection: { xs: 'column', lg: 'row' } }}
      >
        <Box className="flex-[1]">
          <SimpleCard title={t('Total number of orders')}>
            <DoughnutChart
              result={result}
              height="350px"
              color={['#FFAB00', 'gray', '#00B8D9', '#00A76F', '#FF5630']}
            />
          </SimpleCard>
        </Box>
        <Box className="flex-[1]">
          <SimpleCard title="Yearly Sales">
            <LineChart height="350px" color={['#00A76F', '#FFAB00']} />
          </SimpleCard>
        </Box>
      </Box>
      <Box>
        <SimpleCard title={t('Revenue')}>
          <BarCharts />
        </SimpleCard>
      </Box>
      <Box>
        <SimpleCard title={t('Selling products')}>
          <Box pt={5}>
            <BestSellingTable />
          </Box>
        </SimpleCard>
      </Box>
    </Stack>
  )
}
