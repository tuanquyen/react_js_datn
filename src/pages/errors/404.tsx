import React from 'react'
import { Button, Box } from '@mui/material'

const NotFoundPage = () => {
  return (
    <Box className="flex flex-col items-center justify-center h-screen">
      <h1 className="text-4xl font-bold mb-4">404 - Not Found</h1>
      <p className="text-lg mb-8">
        The page you are looking for might not exist or has been moved.
      </p>
      <Button variant="contained" color="primary" href="/dashboard">
        Go to Home
      </Button>
    </Box>
  )
}

export default NotFoundPage
