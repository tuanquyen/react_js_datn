'use client'
import React from 'react'
import AuthenticationComponent from '@/components/Authentication/Authentication'
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import Bg from '@/assets/images/Bg_authentication.jpeg'
import { useForm } from 'react-hook-form'
import { useNavigate } from 'react-router-dom'
import { Typography } from '@mui/material'

interface IFormInput {
  email: string
}

const Forgot = () => {
  const navigate = useNavigate()
  const {
    register,
    handleSubmit,
    formState: { errors }
  } = useForm<IFormInput>()

  const handleRecover = (data: IFormInput) => {
    alert(`Email Recover: ${data.email}`)
  }

  return (
    <Box className="flex flex-col gap-[20px]  md:gap-[26px]">
      <Box className="flex flex-col gap-[15px]  md:gap-[20px]">
        <Box className="text-[25px] font-bold">Forgot Password</Box>
        <Box className="input_component">
          <label className="label">Email</label>
          <input
            {...register('email', {
              required: true,
              pattern: /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/
            })}
            className="input"
          />
          {errors?.email?.type === 'required' && (
            <p className="error">This field is required</p>
          )}
          {errors?.email?.type === 'pattern' && (
            <p className="error">Invalid email</p>
          )}
        </Box>

        <Box className="">
          <Button
            variant="contained"
            className="!bg-primary !normal-case w-[100%] !py-[10px]"
            onClick={handleSubmit(handleRecover)}
          >
            Recover Password
          </Button>
        </Box>
      </Box>
      <hr />

      <Box className="flex flex-col gap-[5px] mg:gap-[10px]">
        <Typography
          variant="body2"
          className="hover:cursor-pointer underline"
          onClick={() => {
            navigate('/login')
          }}
        >
          Already have an account? Login
        </Typography>
      </Box>
    </Box>
  )
}
export default function ForgotPage() {
  return (
    <Box>
      <AuthenticationComponent Bg={Bg}>
        <Forgot />
      </AuthenticationComponent>
    </Box>
  )
}
