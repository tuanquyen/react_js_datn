import TabContext from '@mui/lab/TabContext'
import TabList from '@mui/lab/TabList'
import TabPanel from '@mui/lab/TabPanel'
import { Box, Button, Tab, Typography } from '@mui/material'
import React, { useEffect, useState } from 'react'
import { STYLE_HEADER_TABLE } from '@/assets/styles/style'
import { formatDateTime } from '@/utils/format'
import { useNavigate } from 'react-router-dom'
import TableMUI from '@/components/TableMUI'
import { selectBgColorStatus, selectColorStatus } from '@/utils/select'
import { GetAllOrderAPI } from '@/api/OrderAPI'
import { useTranslation } from 'react-i18next'

const LabelItem = ({ label, value, bg, color }) => {
  return (
    <Box className="flex gap-[10px] items-center justify-center font-bold">
      {label}
      <Box
        sx={{ bgcolor: bg, color: color }}
        className="w-[25px] h-[25px] bg-[#000] text-[#fff] flex items-center justify-center rounded-[5px]"
      >
        {value}
      </Box>
    </Box>
  )
}

export const OrderLayout = ({
  name,
  tabNameStorage,
  user_id
}: {
  name: string
  tabNameStorage: string
  user_id?: number
}) => {
  const [value, setValue] = useState(() => {
    // Lấy giá trị từ localStorage, nếu không có thì sử dụng giá trị mặc định là '1'
    return localStorage.getItem(tabNameStorage) || '1'
  })

  const { t } = useTranslation('translation', { keyPrefix: 'page.Order' })

  const handleChange = (event: React.SyntheticEvent, newValue: string) => {
    setValue(newValue)
  }

  useEffect(() => {
    // Lưu giá trị của Tab vào localStorage khi giá trị thay đổi
    localStorage.setItem(tabNameStorage, value)
  }, [value])

  const navigate = useNavigate()

  const columns = [
    {
      field: 'id',
      sortable: false,
      renderHeader: () => <Box sx={STYLE_HEADER_TABLE}>ID</Box>
    },
    {
      field: 'amount',
      sortable: false,
      minWidth: 150,
      renderHeader: () => <Box sx={STYLE_HEADER_TABLE}>AMOUNT</Box>,
      renderCell: (rowData) => (
        <Box>
          {rowData?.row?.amount.toLocaleString('it-IT', {
            style: 'currency',
            currency: 'VND'
          })}
        </Box>
      )
    },
    {
      field: 'created_at',
      minWidth: 180,
      sortable: false,
      renderHeader: () => <Box sx={STYLE_HEADER_TABLE}>CREATE AT</Box>,
      renderCell: (rowData) => (
        <Box>{formatDateTime(rowData?.row?.created_at)}</Box>
      )
    },
    {
      field: 'state',
      minWidth: 180,
      sortable: false,
      renderHeader: () => <Box sx={STYLE_HEADER_TABLE}>STATE</Box>,
      renderCell: (rowData) => (
        <Box
          className="w-[80px] font-semibold flex items-center justify-center py-[3px] rounded-[4px]"
          sx={{
            color: selectColorStatus(rowData?.row?.state),
            bgcolor: selectBgColorStatus(rowData?.row?.state)
          }}
        >
          {rowData?.row?.state}
        </Box>
      )
    },
    {
      field: 'actions',
      minWidth: 120,
      sortable: false,
      renderCell: (params) => (
        <Box sx={{ display: 'flex', gap: 1 }}>
          <Button
            size="small"
            variant="outlined"
            onClick={() =>
              navigate(`/orders/${params.row.id}`, {
                state: { orderDetail: params.row }
              })
            }
          >
            {t('See details')}
          </Button>
        </Box>
      ),
      renderHeader: () => <Box sx={STYLE_HEADER_TABLE}>ACTIONS</Box>
    }
  ]

  const {
    data: ordersAll = [],
    isLoading: isLoadingAll,
    isError: isErrorAll,
    error: errorAll
  } = GetAllOrderAPI({ state: undefined, user_id: user_id })
  const {
    data: ordersWaiting = [],
    isLoading: isLoadingWaiting,
    isError: isErrorWaiting,
    error: errorWaiting
  } = GetAllOrderAPI({ state: 'WAITING', user_id: user_id })
  const {
    data: ordersConfirm = [],
    isLoading: isLoadingConfirm,
    isError: isErrorConfirm,
    error: errorConfirm
  } = GetAllOrderAPI({ state: 'CONFIRM', user_id: user_id })
  const {
    data: ordersShipping = [],
    isLoading: isLoadingShipping,
    isError: isErrorShipping,
    error: errorShipping
  } = GetAllOrderAPI({ state: 'SHIPPING', user_id: user_id })
  const {
    data: ordersDone = [],
    isLoading: isLoadingDone,
    isError: isErrorDone,
    error: errorDone
  } = GetAllOrderAPI({ state: 'DONE', user_id: user_id })
  const {
    data: ordersCancel = [],
    isLoading: isLoadingCancel,
    isError: isErrorCancel,
    error: errorCancel
  } = GetAllOrderAPI({ state: 'CANCEL', user_id: user_id })

  return (
    <Box>
      <Typography variant="_title">{name}</Typography>
      <Box sx={{ width: '100%', typography: 'body1' }}>
        <TabContext value={value}>
          <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
            <TabList
              sx={{
                '& .MuiTab-root': {
                  padding: '12px 16px',
                  margin: '0px 6px'
                }
              }}
              onChange={handleChange}
              aria-label="lab API tabs example"
            >
              <Tab
                label={
                  <LabelItem
                    label={t('All')}
                    value={ordersAll?.length}
                    color="#fff"
                    bg="#000"
                  />
                }
                value="1"
              />
              <Tab
                label={
                  <LabelItem
                    label={t('Waiting')}
                    value={ordersWaiting?.length}
                    bg="#FFF1D6"
                    color="#B76E00"
                  />
                }
                value="2"
              />
              <Tab
                label={
                  <LabelItem
                    label={t('Confirm')}
                    value={ordersConfirm?.length}
                    bg="#e1f5fe"
                    color="#1976d2"
                  />
                }
                value="3"
              />
              <Tab
                label={
                  <LabelItem
                    label={t('Shipping')}
                    value={ordersShipping?.length}
                    bg="#EDEFF1"
                    color="#637381"
                  />
                }
                value="4"
              />
              <Tab
                label={
                  <LabelItem
                    label={t('Done')}
                    value={ordersDone?.length}
                    bg="#DBF6E5"
                    color="#118D57"
                  />
                }
                value="5"
              />
              <Tab
                label={
                  <LabelItem
                    label={t('Cancel')}
                    value={ordersCancel?.length}
                    bg="#FFE4DE"
                    color="#B71D18"
                  />
                }
                value="6"
              />
            </TabList>
          </Box>
          <TabPanel value="1">
            <TableMUI
              columns={columns}
              rows={ordersAll}
              isLoading={isLoadingAll}
              loadingLabel={t('Loading order list')}
              isError={isErrorAll}
              errorLabel={errorAll?.message}
            />
          </TabPanel>
          <TabPanel value="2">
            <TableMUI
              columns={columns}
              rows={ordersWaiting}
              isLoading={isLoadingWaiting}
              loadingLabel={t('Loading order list (Waiting)')}
              isError={isErrorWaiting}
              errorLabel={errorWaiting?.message}
            />
          </TabPanel>
          <TabPanel value="3">
            <TableMUI
              columns={columns}
              rows={ordersConfirm}
              isLoading={isLoadingConfirm}
              loadingLabel={t('Loading order list (Confirm)')}
              isError={isErrorConfirm}
              errorLabel={errorConfirm?.message}
            />
          </TabPanel>
          <TabPanel value="4">
            <TableMUI
              columns={columns}
              rows={ordersShipping}
              isLoading={isLoadingShipping}
              loadingLabel={t('Loading order list (Shipping)')}
              isError={isErrorShipping}
              errorLabel={errorShipping?.message}
            />
          </TabPanel>
          <TabPanel value="5">
            <TableMUI
              columns={columns}
              rows={ordersDone}
              isLoading={isLoadingDone}
              loadingLabel={t('Loading order list (Done)')}
              isError={isErrorDone}
              errorLabel={errorDone?.message}
            />
          </TabPanel>
          <TabPanel value="6">
            <TableMUI
              columns={columns}
              rows={ordersCancel}
              isLoading={isLoadingCancel}
              loadingLabel={t('Loading order list (Cancel)')}
              isError={isErrorCancel}
              errorLabel={errorCancel?.message}
            />
          </TabPanel>
        </TabContext>
      </Box>
    </Box>
  )
}

export default function OrderPage() {
  const { t } = useTranslation('translation', { keyPrefix: 'page.Order' })
  return <OrderLayout name={t('Order Page')} tabNameStorage="currentTabOrder" />
}
