'use client'

import React from 'react'
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import MainLayout from '@/components/MainLayout'

const SettingComponent = () => {
  return (
    <>
      <Box
        sx={{
          display: 'flex',
          flexDirection: 'column',
          gap: '15px',
          maxWidth: '500px'
        }}
      >
        <Box className="input_component">
          <label className="label">Number of images per product</label>
          <input className="input" />
        </Box>
        <Box className="input_component">
          <label className="label">Default currency</label>
          <input className="input" />
        </Box>
        <Box className="input_component">
          <label className="label">Default time zone</label>
          <input className="input" />
        </Box>
        <Box className="input_component">
          <label className="label">Default date format</label>
          <input className="input" />
        </Box>
        <Box className="input_component">
          <label className="label">Receipt size</label>
          <input className="input" />
        </Box>

        <Box>
          <Button variant="contained" color="success">
            Update
          </Button>
        </Box>
      </Box>
    </>
  )
}

export default function SettingPage() {
  return <MainLayout content={<SettingComponent />} />
}
