import React from 'react'
import { Box, Stack, Typography } from '@mui/material'
import { useTranslation } from 'react-i18next'
import TableMUI from '@/components/TableMUI'
import {
  DeleteOutlineOutlined,
  DriveFileRenameOutlineOutlined,
  LoupeOutlined
} from '@mui/icons-material'
import { useNavigate } from 'react-router-dom'
import { formatDateTime } from '@/utils/format'
import { STYLE_HEADER_TABLE } from '@/assets/styles/style'
import { GetAllStaffAPI } from '@/api/StaffAPI'

const StaffTable = () => {
  const navigate = useNavigate()
  const { t } = useTranslation('translation', { keyPrefix: 'page.Staff' })
  const columns = [
    {
      field: 'id',
      maxWidth: 30,
      sortable: false,
      renderHeader: () => <Box sx={STYLE_HEADER_TABLE}>ID</Box>
    },
    {
      field: 'image',
      flex: 1,
      sortable: false,
      renderHeader: () => <Box sx={STYLE_HEADER_TABLE}>{t('Image')}</Box>,
      renderCell: (params) => (
        <Box>
          <img src={params.row.image} width={50} height={50} alt="" />
        </Box>
      )
    },
    {
      field: 'name',
      minWidth: 150,
      flex: 1,
      sortable: false,
      renderHeader: () => <Box sx={STYLE_HEADER_TABLE}>{t('Staff Name')}</Box>
    },

    {
      field: 'email',
      minWidth: 200,
      flex: 1,
      sortable: false,
      renderHeader: () => <Box sx={STYLE_HEADER_TABLE}>{t('Email')}</Box>
    },
    {
      field: 'phone_number',
      minWidth: 150,
      flex: 1,
      sortable: false,
      renderHeader: () => <Box sx={STYLE_HEADER_TABLE}>{t('Email')}</Box>
    },
    {
      field: 'password_change_at',
      minWidth: 150,
      flex: 1,
      sortable: false,
      renderHeader: () => (
        <Box sx={STYLE_HEADER_TABLE}>{t('Password change at')}</Box>
      ),
      renderCell: (rowData) => (
        <Box>{formatDateTime(rowData.row.password_change_at)}</Box>
      )
    },
    {
      field: 'last_login_at',
      minWidth: 150,
      flex: 1,
      sortable: false,
      renderHeader: () => (
        <Box sx={STYLE_HEADER_TABLE}>{t('Last login at')}</Box>
      ),
      renderCell: (rowData) => (
        <Box>{formatDateTime(rowData.row.last_login_at)}</Box>
      )
    },
    {
      field: 'registry_at',
      minWidth: 150,
      flex: 1,
      sortable: false,
      renderHeader: () => <Box sx={STYLE_HEADER_TABLE}>{t('Registry at')}</Box>,
      renderCell: (rowData) => (
        <Box>{formatDateTime(rowData.row.registry_at)}</Box>
      )
    },

    {
      field: 'actions',
      flex: 1,
      sortable: false,
      minWidth: 200,
      renderCell: (params) => (
        <Box sx={{ display: 'flex', gap: 3 }}>
          <DriveFileRenameOutlineOutlined
            style={{ cursor: 'pointer' }}
            // onClick={() => handleEdit(params.row.id)}
            onClick={() => alert(`Edit: ${params.row.id}`)}
          />
          <DeleteOutlineOutlined
            style={{ cursor: 'pointer' }}
            // onClick={() => handleDelete(params.row.id, params.row.title)}
            onClick={() => alert(`Delete: ${params.row.id}`)}
          />
          <LoupeOutlined
            style={{ cursor: 'pointer' }}
            // onClick={() => handleDetail(params.row)}
            onClick={() => navigate(`/staffs/${params.row.id}`)}
          />
        </Box>
      ),
      renderHeader: () => <Box sx={STYLE_HEADER_TABLE}></Box>
    }
  ]

  const { data: allStaff } = GetAllStaffAPI()

  return <TableMUI columns={columns} rows={allStaff} />
}

export default function StaffPage() {
  const { t } = useTranslation('translation', { keyPrefix: 'page.Staff' })
  return (
    <Stack gap={5}>
      <Typography variant="_title">{t('Staff Page')}</Typography>
      <StaffTable />
    </Stack>
  )
}
