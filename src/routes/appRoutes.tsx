import { ReactNode } from 'react'
import {
  CatalogIcon,
  CustomerIcon,
  DashboardIcon,
  OrderIcon,
  StaffIcon
} from '@/assets/icons'
import ComponentPageLayout from '@/components/ComponentPageLayout'
import DashboardPage from '@/pages/dashboard/page'
import ProductPage from '@/pages/catalogs/products/page'
import CategoryPage from '@/pages/catalogs/categories/page'
import CouponsPage from '@/pages/catalogs/coupons/page'
import OrderPage from '@/pages/orders/page'
import CustomerPage from '@/pages/customers/page'
import AboutUsPage from '@/pages/about/AboutUs'
import StaffPage from '@/pages/staffs/page'
import CreateStaffPage from '@/pages/staffs/createStaff/createStaff'

type RouteType = {
  element: ReactNode
  state: string
  index?: boolean
  path?: string
  child?: RouteType[]
  sidebarProps?: {
    keyTextI18Next?: string
    displayText: string
    icon?: ReactNode
  }
}

const appRoutes: RouteType[] = [
  {
    path: '/dashboard',
    element: <DashboardPage />,
    state: 'dashboard',
    sidebarProps: {
      keyTextI18Next: 'Dashboard',
      displayText: 'Dashboard',
      icon: <DashboardIcon />
    }
  },
  {
    path: '/catalogs',
    element: <ComponentPageLayout />,
    state: 'catalogs',
    sidebarProps: {
      keyTextI18Next: 'Catalogs',
      displayText: 'Catalogs',
      icon: <CatalogIcon />
    },
    child: [
      {
        path: '/catalogs/products',
        element: <ProductPage />,
        state: 'catalogs.products',
        sidebarProps: {
          keyTextI18Next: 'Products',
          displayText: 'Products'
        }
      },
      {
        path: '/catalogs/categories',
        element: <CategoryPage />,
        state: 'catalogs.categories',
        sidebarProps: {
          keyTextI18Next: 'Categories',
          displayText: 'Categories'
        }
      },
      {
        path: '/catalogs/coupons',
        element: <CouponsPage />,
        state: 'catalogs.coupons',
        sidebarProps: {
          keyTextI18Next: 'Coupons',
          displayText: 'Coupons'
        }
      }
    ]
  },
  {
    path: '/orders',
    element: <OrderPage />,
    state: 'orders',
    sidebarProps: {
      displayText: 'Orders',
      keyTextI18Next: 'Orders',
      icon: <OrderIcon />
    }
  },
  {
    path: '/customer',
    element: <CustomerPage />,
    state: 'customer',
    sidebarProps: {
      displayText: 'Customer',
      keyTextI18Next: 'Customers',
      icon: <CustomerIcon />
    }
  },
  {
    path: '',
    element: <ComponentPageLayout />,
    state: 'staff',
    sidebarProps: {
      displayText: 'Staffs',
      keyTextI18Next: 'Staffs',
      icon: <StaffIcon />
    },
    child: [
      {
        path: '/staff/list',
        element: <StaffPage />,
        state: 'staff.list',
        sidebarProps: {
          displayText: 'Staff List',
          keyTextI18Next: 'List'
        }
      },
      {
        path: '/staffs/create',
        element: <CreateStaffPage />,
        state: 'staff.create',
        sidebarProps: {
          displayText: 'Create',
          keyTextI18Next: 'Create'
        }
      }
    ]
  },
  {
    path: '/about',
    element: <AboutUsPage />,
    state: 'about',
    sidebarProps: {
      displayText: 'About Us',
      keyTextI18Next: 'About Us',
      icon: <StaffIcon />
    }
  }
]

export default appRoutes
