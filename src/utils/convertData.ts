import { GetAllCategoryAPIType } from '@/api/CategoryAPI'

interface convertCategoryType {
  id: number
  name: string
  image?: string
}

// Lấy danh sách tất cả category từ api getAllCategory
export function convertCategory(categories?: GetAllCategoryAPIType[]) {
  const result: convertCategoryType[] = []
  function processCategory(category) {
    result.push({
      id: category.id,
      name: category.name,
      image: category.image
    })

    if (category.child_categories && category.child_categories.length > 0) {
      category.child_categories.forEach((childCategory) => {
        processCategory(childCategory)
      })
    }
  }
  if (categories) {
    categories?.forEach((category) => {
      processCategory(category)
    })

    return result
  }
}

// 1 array total + 1 array name => 1 array id
export function convertArrayIds(array, arrayNames) {
  const arrayIds = []

  for (const title of arrayNames) {
    const result = array?.find((cat) => cat.title === title)
    if (result !== undefined) {
      arrayIds.push(result.id as never)
    }
  }
  return arrayIds
}

export function convertArrayNames(
  array: { id: number; name: string }[],
  arrayIds: number[]
): string[] {
  const arrayNames: string[] = []

  for (const id of arrayIds) {
    const result = array?.find((cat) => cat.id === id)
    if (result !== undefined) {
      arrayNames.push(result.name)
    }
  }
  return arrayNames
}

// Chuyển 1 id sang name
export function ConvertIdToName(array, id) {
  const result = array?.find((cat) => cat.id === id)
  if (result !== undefined) {
    return result.name
  }
  return ''
}
