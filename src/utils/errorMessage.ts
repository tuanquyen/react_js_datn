import { AxiosError } from 'axios'
import { toast } from 'react-toastify'

export const toastErrorMessage = (err, mes?: string) => {
  console.log('err:', err)
  if (err instanceof AxiosError) {
    if (Array.isArray(err?.response?.data.message)) {
      toast.error(err.response.data.message[0].message)
    } else if (err?.response?.data.message) {
      toast.error(err.response.data.message)
    } else {
      toast.error(err?.message)
    }
  } else {
    toast.error(mes)
  }
}
