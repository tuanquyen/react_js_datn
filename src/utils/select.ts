export const selectColorStatus = (state) => {
  switch (state) {
    case 'WAITING':
      return '#B76E00'
    case 'CONFIRM':
      return '#1976d2'
    case 'SHIPPING':
      return '#637381'
    case 'DONE':
      return '#118D57'
    case 'CANCEL':
      return '#B71D18'
    default:
      return '#fff'
  }
}

export const selectBgColorStatus = (state) => {
  switch (state) {
    case 'WAITING':
      return '#FFF1D6'
    case 'CONFIRM':
      return '#e1f5fe'
    case 'SHIPPING':
      return '#EDEFF1'
    case 'DONE':
      return '#DBF6E5'
    case 'CANCEL':
      return '#FFE4DE'
    default:
      return '#000'
  }
}
