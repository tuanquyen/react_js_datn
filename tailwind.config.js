/** @type {import('tailwindcss').Config} */
export default {
  darkMode: 'class',
  content: ['./index.html', './src/**/*.{js,ts,jsx,tsx}'],
  plugins: [],
  corePlugins: {
    preflight: false //fix button, [type='button'],[type='reset'],[type='submit'] {...}
  }
}
